debug = false

main = {}

require('agent')



function love.load()
    player = {
        grid_x = start.grid_x, --start
        grid_y = start.grid_y, --start
        act_x = 16,
        act_y = 16,
        speed = 5,
        orientation = 0,
        act_orient = 0
    }



      -- Load images (global assets)
  img_fn = {"robot"}
  imgs = {}
  for _,v in ipairs(img_fn) do
    imgs[v]=love.graphics.newImage("assets/"..v..".png")
  end

  -- load sounds
  ups = love.audio.newSource( "assets/ups.ogg" , "static" )
  yeah = love.audio.newSource( "assets/yeah.wav" , "static" )

  -- initialer Status
  agentAktiv = false

  agent.load()

end


function love.update(dt)
    player.act_y = player.act_y - ((player.act_y - player.grid_y) * player.speed * dt)
    player.act_x = player.act_x - ((player.act_x - player.grid_x) * player.speed * dt)

    player.act_orient = player.act_orient - ((player.act_orient - player.orientation) * dt * player.speed ) 
    if player.act_orient == 4 then
        player.act_orient = 0
        player.orientation = 0
    end
    if agentAktiv then
        agent.update(dt)
    end
end


function love.draw()
    for y=1, #map do
        for x=1, #map[y] do
            if map[y][x] == 1 then
            	love.graphics.setColor(50,50,50)
                love.graphics.rectangle("fill", x * scale, y * scale, scale, scale)
            else 
            	love.graphics.setColor(255,255,255)
            	love.graphics.rectangle("fill", x * scale+1, y * scale+1, scale-2, scale-2)
            end
        end
    end
    love.graphics.setColor(0,200,0)
    love.graphics.setPointStyle("smooth")
    love.graphics.setPointSize(scale-2)
    love.graphics.point(target.grid_x*scale + scale /2, target.grid_y*scale + scale / 2)

    love.graphics.setColor(200,0,0)
    love.graphics.setPointStyle("smooth")
    love.graphics.setPointSize(scale-2)
    love.graphics.point(start.grid_x*scale + scale /2, start.grid_y*scale + scale / 2)

    love.graphics.setColor(255,255,255)
    love.graphics.draw(imgs["robot"], player.act_x*scale + 18, player.act_y*scale + 16, math.rad(90*player.act_orient), 1, 1, 16, 16)

    love.graphics.print("x:" .. player.grid_x, 32,580)
    love.graphics.print("y:" .. player.grid_y, 62,580)
    love.graphics.print("o:" .. player.orientation*90 .. "° (" .. player.orientation%4 * 90 .. "°)", 92,580)

    if agentAktiv then
        agent.draw()
    end

end


function love.keypressed(key)
    if not agentAktiv then
        if key == "up" then
            main.move("forward")

        elseif key == "down" then
            main.move("backward")

        elseif key == "left" then
            main.rotate("left")

        elseif key == "right" then
            main.rotate("right")
        end
    end
    if key == "#" then
        map = map2    
    elseif key == "+" then
    	debug = not debug
    elseif key == "escape" then
      love.event.push("quit")
    elseif key == " " then
      agentAktiv = not agentAktiv
    end

    if agentAktiv then
        agent.keypressed(key)
    end

end



-- custum functions

function main.testMap(x, y)
	if player.grid_y + y <= 0 then -- check Rand oben	
		return false
	elseif player.grid_x + x <= 0 then -- check Rand links
		return false
	elseif player.grid_x + x > table.getn(map[player.grid_y]) then -- check Rand rechts
		return false
	elseif player.grid_y + y > table.getn(map) then -- check Rand unten
		return false
	elseif map[(player.grid_y) + y][(player.grid_x) + x] == 1 then  -- check Wand
        return false
    elseif player.grid_x+x==target.grid_x and player.grid_y+y==target.grid_y then -- am ZIEL?
    	love.audio.play(yeah)
    	return true
    end
    return true
end


function main.move(direction)

    x, y, orientation = main.orientationCoords(player, direction)
    
	if main.testMap(x, y) then
              player.grid_y = player.grid_y + y
		player.grid_x = player.grid_x + x
	else 
        love.audio.play(ups)
        return 0, 0
    end
    return x, y, orientation

end


function main.rotate(direction)
	if direction == "left" then
		player.orientation = player.orientation - 1
		-- if player.orientation < 0 then
		-- 	player.orientation = 3
		-- end
	elseif direction == "right" then
		player.orientation = player.orientation + 1
		-- if player.orientation > 3 then
		-- 	player.orientation = 0
		-- end
	end
end


function main.orientationCoords(player, direction)
    if player.orientation%4 == 0 then
        x, y = 0, -1
    elseif player.orientation%4 == 1 then
        x, y = 1, 0
    elseif player.orientation%4 == 2 then
        x, y = 0, 1
    elseif player.orientation%4 == 3 then
        x, y = -1, 0
    end

    if direction == "backward" then
        x, y = -x, -y
    end

    z = player.orientation%4

    return x, y, z

end


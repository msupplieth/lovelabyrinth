agent = {}

function agent.load()
	-- array Erfahrungen initialisieren
	agent.map = {}
	for i=-16, 16 do
		agent.map[i]={}
		for j=-16, 16 do
      		agent.map[i][j] = 1
      	end
    end

    agent.mapcolor = {
    	{r=255,g=255,b=255}, --1: weiß:	nicht begangen
    	{r=55,g=55,b=55},    --2: grau: einmal begangen
    	{r=200, g=0, b=0},   --3: rot:	2 mal begangen -> gesperrt
    	{r=0,g=200,b=0},	 --4: grün: Kreuzung frontal
    	{r=200,g=200,b=0},	 --4: gelb: Kreuzung seitlich
    	{r=0,g=0,b=200}		 --5: blau: Hindernis
	}

	agent.timer = 0
	agent.interval = 0.5

	agent.pos = {
		x = 0,
		y = 0,
		xLast = 0,
		yLast = 0,
		orientationLast = 0
	}

	agent.steps = 0

end


function agent.draw()
    love.graphics.print("Agent unterwegs...", 600,580)
    love.graphics.print("Schritte: ".. agent.steps, 750,580)

    for y=-16, #agent.map do
        for x=-16, #agent.map[y] do
            z=agent.map[x][y]
            if not (x==0 and y==0) then
            	love.graphics.setColor(agent.mapcolor[z].r,agent.mapcolor[z].g,agent.mapcolor[z].b)
            else
            	love.graphics.setColor(200,0,0)
            end
            love.graphics.rectangle("fill", x * 15 + 900, y * 15 +280, 13, 13)
        end
    end
end


function agent.update(dt)
	if not (player.grid_x==target.grid_x and player.grid_y==target.grid_y) then
		agent.timer = agent.timer + dt
		if agent.timer >= agent.interval then
			agent.step()
			agent.timer = agent.timer - agent.interval
		end
	end

end


function agent.keypressed(key)
	if key == "+" then
		agent.interval = agent.interval - 0.1
	elseif key == "-" then
		agent.interval = agent.interval + 0.1
	end
end


function agent:step()

	agent.steps = agent.steps+1

	local wallLeft		= agent.checkWall("left")
	local wallRight		= agent.checkWall("right")
	local wallAhead 	= agent.checkWall("ahead")

	local statusLeft	= agent.getMap(agent.look("left"))
	local statusRight	= agent.getMap(agent.look("right"))
	local statusAhead	= agent.getMap(agent.look("ahead"))

	if not wallAhead then

		stepx, stepy = main.move("forward")
	
		agent.pos.x = agent.pos.x + stepx
		agent.pos.y = agent.pos.y + stepy

		if (agent.getMap() < 3 ) then
			agent.setMap()
		end

		local passageLeft	= agent.checkPassage("left")
		local passageRight	= agent.checkPassage("right")

	else -- Hindernis voraus
		agent.setMap(4)
		if not wallLeft and not wallRight then  -- T-Kreuzung
			if statusLeft < statusRight then
				main.rotate("left") 
			elseif statusRight < statusLeft then
				main.rotate("right")
			else 
				local tendenz 	= 50 + player.orientation * 5 -- Tendenz zu Orientierung gegen 0 Grad (zufällig)
				local maxrand	= 50 + tendenz
				if math.random(maxrand) < tendenz then   
					main.rotate("left")
				else 
					main.rotate("right")
				end
			end
		elseif not wallLeft then
			main.rotate("left")
		elseif not wallRight then
			main.rotate("right")
		else 
			main.rotate("right")
			main.rotate("right")
		end
	end

	agent.pos.xLast = agent.pos.x
	agent.pos.yLast = agent.pos.y
	agent.pos.orientationLast = player.orientation

end




function agent.setMap(value, x,y) --relativ zur Position
	x = x or 0
	y = y or 0
	value = value or agent.getMap(x,y) + 1
	agent.map[agent.pos.x+x][agent.pos.y+y] = value
	return true
end


function agent.getMap(x,y) -- relativ zur Position
	x = x or 0
	y = y or 0
	local value = agent.map[agent.pos.x+x][agent.pos.y+y]
	return value
end


function agent.look(direction)
	x, y = main.orientationCoords(player)

	if direction == "ahead" then

		x = x
		y = y

	elseif direction == "left" then -- Vektorrotation positiv 90 Grad

		local xx = x
		local yy = y 
		x = yy
		y = xx *-1


	elseif direction == "right" then  -- Vektorrotation negativ 90 Grad

		local xx = x
		local yy = y 
		x = yy *-1
		y = xx 
			
	end
	return x, y
end


function agent.checkWall(direction)
	x,y = agent.look(direction)
	if not main.testMap(x, y) then -- Hindernis erkannt
		agent.setMap(6,x,y)   -- In Erfahrungen speichern
		return 1 -- Hindernis
	else 
		return false -- freie Fahrt voraus
	end
end

function agent.checkPassage(direction)
	local x,y = agent.look(direction)
	local old = agent.map[agent.pos.xLast+x][agent.pos.yLast+y]
	local now = agent.map[agent.pos.x+x][agent.pos.y+y]

	if old == 6 and not agent.checkWall(direction) then
		agent.setMap(5)
		return true
	else 
		return false
	end

end